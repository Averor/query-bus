<?php declare(strict_types=1);

use Averor\MessageBus\QueryBus\Contract\Query;
use Averor\MessageBus\QueryBus\Contract\QueryHandler;
use Averor\MessageBus\QueryBus\Middleware\LoggingMiddleware;
use Averor\MessageBus\QueryBus\Middleware\QueryDispatchingMiddleware;
use Averor\MessageBus\QueryBus\QueryBus;
use Averor\MessageBus\Resolver\CallableMapResolver;
use Averor\SimpleLogger\Logger;
use Psr\Log\LogLevel;

require __DIR__ . '/../vendor/autoload.php';

class SomeQuery implements Query
{
    public $id, $name;
    public function __construct(int $id, string $name)
    {
        $this->id = $id;
        $this->name = $name;
    }
}

class SomeQueryHandler implements QueryHandler
{
    public function __invoke(SomeQuery $query)
    {
        return sprintf(
            'Some result retrieved for query of id: %d and name: %s',
            $query->id,
            $query->name
        );
    }
}

$map = [
    SomeQuery::class => new SomeQueryHandler()
];

$bus = new QueryBus([
    new LoggingMiddleware(
        new Logger('php://output'),
        LogLevel::INFO
    ),
    new QueryDispatchingMiddleware(
        new CallableMapResolver($map)
    ),
]);

$result = $bus->dispatch(
    new SomeQuery($id=34, $name='foo')
);

printf("Query (%d:%s) result: %s\n", $id, $name, $result);
