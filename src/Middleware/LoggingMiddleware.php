<?php declare(strict_types=1);

namespace Averor\MessageBus\QueryBus\Middleware;

use Averor\MessageBus\QueryBus\Contract\Query;
use Averor\MessageBus\QueryBus\Contract\QueryBusMiddleware;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;

/**
 * Class LoggingMiddleware
 *
 * @package Averor\MessageBus\QueryBus\Middleware
 * @author Averor <averor.dev@gmail.com>
 */
class LoggingMiddleware implements QueryBusMiddleware
{
    /** @var LoggerInterface */
    protected $logger;

    /** @var string */
    protected $level;

    public function __construct(LoggerInterface $logger, ?string $level = LogLevel::INFO)
    {
        $this->logger = $logger;
        $this->level = $level;
    }

    public function execute(Query $query, callable $next)
    {
        $this->logger->log(
            $this->level,
            sprintf(
                "Dispatching query %s",
                get_class($query)
            ),
            [
                'message' => $query
            ]
        );

        $result = $next($query);

        $this->logger->log(
            $this->level,
            sprintf(
                "Query %s dispatched",
                get_class($query)
            ),
            [
                'message' => $query
            ]
        );

        return $result;
    }
}
