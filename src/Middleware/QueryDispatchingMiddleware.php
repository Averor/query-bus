<?php declare(strict_types=1);

namespace Averor\MessageBus\QueryBus\Middleware;

use Averor\MessageBus\Contract\OneToOneResolver;
use Averor\MessageBus\QueryBus\Contract\Query;
use Averor\MessageBus\QueryBus\Contract\QueryDispatcher;

/**
 * Class QueryDispatchingMiddleware
 *
 * @package Averor\MessageBus\QueryBus\Middleware
 * @author Averor <averor.dev@gmail.com>
 */
class QueryDispatchingMiddleware implements QueryDispatcher
{
    /** @var OneToOneResolver */
    protected $resolver;

    public function __construct(OneToOneResolver $resolver)
    {
        $this->resolver = $resolver;
    }

    public function execute(Query $query, callable $next)
    {
        /** @var Callable $handler */
        $handler = $this->resolver->resolve($query);

        if (is_callable($handler)) {
            $result = $handler($query);
        }

        $next($query);

        return $result ?? null;
    }
}
