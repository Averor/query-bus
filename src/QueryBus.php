<?php declare(strict_types=1);

namespace Averor\MessageBus\QueryBus;

use Averor\MessageBus\Contract\Message;
use Averor\MessageBus\QueryBus\Contract\QueryBusMiddleware;

/**
 * Class QueryBus
 *
 * @package Averor\MessageBus\QueryBus
 * @author Averor <averor.dev@gmail.com>
 */
class QueryBus
{
    protected $middlewares;

    public function __construct(array $middlewares = [])
    {
        foreach (array_reverse($middlewares) as $middleware) {
            $this->prependMiddleware($middleware);
        }
    }

    public function appendMiddleware(QueryBusMiddleware $middleware)
    {
        array_unshift($this->middlewares, $middleware);
    }

    public function prependMiddleware(QueryBusMiddleware $middleware)
    {
        $this->middlewares[] = $middleware;
    }

    public function dispatch(Message $message)
    {
        return call_user_func($this->execute(), $message);
    }

    public function execute()
    {
        $next = function () {};

        reset($this->middlewares);

        while ($middleware = current($this->middlewares)) {

            $next = function ($message) use ($middleware, $next) {
                return $middleware->execute($message, $next);
            };

            next($this->middlewares);
        }

        return $next;
    }
}
