<?php declare(strict_types=1);

namespace Averor\MessageBus\QueryBus\Contract;

use Averor\MessageBus\Contract\Message;

/**
 * Interface Query
 *
 * @package Averor\MessageBus\QueryBus\Contract
 * @author Averor <averor.dev@gmail.com>
 */
interface Query extends Message
{
}
