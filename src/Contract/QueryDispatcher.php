<?php declare(strict_types=1);

namespace Averor\MessageBus\QueryBus\Contract;

/**
 * Interface QueryDispatcher
 *
 * @package Averor\MessageBus\QueryBus\Contract
 * @author Averor <averor.dev@gmail.com>
 */
interface QueryDispatcher extends QueryBusMiddleware
{
}
