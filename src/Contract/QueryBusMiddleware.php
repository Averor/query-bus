<?php declare(strict_types=1);

namespace Averor\MessageBus\QueryBus\Contract;

/**
 * Interface QueryBusMiddleware
 *
 * @package Averor\MessageBus\QueryBus\Contract
 * @author Averor <averor.dev@gmail.com>
 */
interface QueryBusMiddleware
{
    /**
     * @param Query $message
     * @param callable $next
     * @return mixed
     */
    public function execute(Query $message, callable $next);
}
