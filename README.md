# Averor/Query-Bus

[![pipeline status](https://gitlab.com/Averor/query-bus/badges/master/pipeline.svg)](https://gitlab.com/Averor/query-bus/commits/master)
[![coverage report](https://gitlab.com/Averor/query-bus/badges/master/coverage.svg)](https://gitlab.com/Averor/query-bus/commits/master)
[![MIT License](https://img.shields.io/badge/license-MIT-brightgreen.svg)](https://gitlab.com/Averor/query-bus/blob/master/LICENSE)
[![Semver](http://img.shields.io/SemVer/2.0.0-pre-alpha.png)](http://semver.org/spec/v2.0.0.html)

Simple [Averor/message-bus](https://gitlab.com/Averor/message-bus) extension aimed to help dealing with cqrs query part.

## Install using Composer:

`composer require averor/query-bus`

## Documentation

Will be surely written... someday. As for now - just take a look at [examples/](https://gitlab.com/Averor/query-bus/tree/master/examples) 
or [tests/](https://gitlab.com/Averor/query-bus/tree/master/tests)

### QueryBus

_MessageBus_ is not able to return value from handler, so that's where _QueryBus_ comes to the rescue.
It uses some contracts from _MessageBus_ but **middlewares are not interchangeable**.

> \Averor\MessageBus\QueryBus\Contract\QueryBusMiddleware 

introduced to assure that no base _MessageBus_ middleware would be registered. 

## Testing with PHPUnit (>=7.0)
`$ ./vendor/bin/phpunit ./tests` 
