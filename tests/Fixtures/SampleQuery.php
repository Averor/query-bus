<?php declare(strict_types=1);

namespace Averor\MessageBus\QueryBus\Tests\Fixtures;

use Averor\MessageBus\QueryBus\Contract\Query;

/**
 * Class SampleQuery
 *
 * @package Averor\MessageBus\QueryBus\Tests\Fixtures
 * @author Averor <averor.dev@gmail.com>
 */
class SampleQuery implements Query
{
    /** @var int */
    public $id;

    /** @var string */
    public $name = self::class;

    public function __construct(int $id)
    {
        $this->id = $id;
    }
}
