<?php declare(strict_types=1);

namespace Averor\MessageBus\QueryBus\Tests\Fixtures;

use Averor\MessageBus\QueryBus\Contract\Query;

/**
 * Class SampleQueryWithTestContainer
 *
 * @package Averor\MessageBus\QueryBus\Tests\Fixtures
 * @author Averor <averor.dev@gmail.com>
 */
class SampleQueryWithTestContainer implements Query
{
    /** @var int */
    public $id;

    /** @var string */
    public $name = self::class;

    /** @var \ArrayObject */
    public $container;

    public function __construct(int $id, \ArrayObject $container)
    {
        $this->id = $id;
        $this->container = $container;
    }

    public function addContainerEntry(string $entry) : void
    {
        $this->container->append($entry);
    }
}
