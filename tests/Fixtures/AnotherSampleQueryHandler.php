<?php declare(strict_types=1);

namespace Averor\MessageBus\QueryBus\Tests\Fixtures;

use Averor\MessageBus\QueryBus\Contract\QueryHandler;

/**
 * Class AnotherSampleQueryHandler
 *
 * @package Averor\MessageBus\QueryBus\Tests\Fixtures
 * @author Averor <averor.dev@gmail.com>
 */
class AnotherSampleQueryHandler implements QueryHandler
{
    public function __invoke(AnotherSampleQuery $query)
    {
        return sprintf(
            'Another sample result retrieved for query of id: %d and name: %s',
            $query->id,
            $query->name
        );
    }
}
