<?php declare(strict_types=1);

namespace Averor\MessageBus\QueryBus\Tests\Fixtures\SampleMiddleware;

use Averor\MessageBus\QueryBus\Contract\Query;
use Averor\MessageBus\QueryBus\Contract\QueryBusMiddleware;

/**
 * Class BazSampleMiddleware
 *
 * @package Averor\MessageBus\QueryBus\Tests\Fixtures\SampleMiddleware
 * @author Averor <averor.dev@gmail.com>
 */
class BazSampleMiddleware implements QueryBusMiddleware
{
    public function execute(Query $query, callable $next)
    {
        if (method_exists($query, 'addContainerEntry')) {
            $query->addContainerEntry('Baz::start');
        }

        $result = $next($query);

        if (method_exists($query, 'addContainerEntry')) {
            $query->addContainerEntry('Baz::end');
        }

        return $result;
    }
}
