<?php declare(strict_types=1);

namespace Averor\MessageBus\QueryBus\Tests\Fixtures\SampleMiddleware;

use Averor\MessageBus\QueryBus\Contract\Query;
use Averor\MessageBus\QueryBus\Contract\QueryBusMiddleware;

/**
 * Class BatSampleMiddleware
 *
 * @package Averor\MessageBus\QueryBus\Tests\Fixtures\SampleMiddleware
 * @author Averor <averor.dev@gmail.com>
 */
class BatSampleMiddleware implements QueryBusMiddleware
{
    public function execute(Query $query, callable $next)
    {
        if (method_exists($query, 'addContainerEntry')) {
            $query->addContainerEntry('Bat::start');
        }

        $result = $next($query);

        if (method_exists($query, 'addContainerEntry')) {
            $query->addContainerEntry('Bat::end');
        }

        return $result;
    }
}
