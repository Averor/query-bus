<?php declare(strict_types=1);

namespace Averor\MessageBus\QueryBus\Tests\Fixtures\SampleMiddleware;

use Averor\MessageBus\QueryBus\Contract\Query;
use Averor\MessageBus\QueryBus\Contract\QueryBusMiddleware;

/**
 * Class BarSampleMiddleware
 *
 * @package Averor\MessageBus\QueryBus\Tests\Fixtures\SampleMiddleware
 * @author Averor <averor.dev@gmail.com>
 */
class BarSampleMiddleware implements QueryBusMiddleware
{
    public function execute(Query $query, callable $next)
    {
        if (method_exists($query, 'addContainerEntry')) {
            $query->addContainerEntry('Bar::start');
        }

        $result = $next($query);

        if (method_exists($query, 'addContainerEntry')) {
            $query->addContainerEntry('Bar::end');
        }

        return $result;
    }
}
