<?php declare(strict_types=1);

namespace Averor\MessageBus\QueryBus\Tests\Fixtures;

use Averor\MessageBus\QueryBus\Contract\QueryHandler;

/**
 * Class SampleQueryHandler
 *
 * @package Averor\MessageBus\QueryBus\Tests\Fixtures
 * @author Averor <averor.dev@gmail.com>
 */
class SampleQueryHandler implements QueryHandler
{
    public function __invoke(SampleQuery $query)
    {
        return sprintf(
            'Sample result retrieved for query of id: %d and name: %s',
            $query->id,
            $query->name
        );
    }
}