<?php declare(strict_types=1);

namespace Averor\MessageBus\QueryBus\Tests;

use Averor\MessageBus\QueryBus\Middleware\QueryDispatchingMiddleware;
use Averor\MessageBus\QueryBus\QueryBus;
use Averor\MessageBus\QueryBus\Tests\Fixtures\SampleMiddleware\BarSampleMiddleware;
use Averor\MessageBus\QueryBus\Tests\Fixtures\SampleMiddleware\BatSampleMiddleware;
use Averor\MessageBus\QueryBus\Tests\Fixtures\SampleMiddleware\BazSampleMiddleware;
use Averor\MessageBus\QueryBus\Tests\Fixtures\SampleMiddleware\FooSampleMiddleware;
use Averor\MessageBus\QueryBus\Tests\Fixtures\SampleQuery;
use Averor\MessageBus\QueryBus\Tests\Fixtures\SampleQueryHandler;
use Averor\MessageBus\QueryBus\Tests\Fixtures\SampleQueryWithTestContainer;
use Averor\MessageBus\Resolver\CallableMapResolver;
use PHPUnit\Framework\TestCase;

/**
 * Class QueryBusTest
 *
 * @package Averor\MessageBus\QueryBus\Tests
 * @author Averor <averor.dev@gmail.com>
 */
class QueryBusTest extends TestCase
{
    public function test_it_execute_middlewares_in_correct_order() : void
    {
        $queryBus = new QueryBus([
            new BazSampleMiddleware(),
            new BarSampleMiddleware()
        ]);

        $queryBus->appendMiddleware(new BatSampleMiddleware());
        $queryBus->prependMiddleware(new FooSampleMiddleware());

        $expected = [
            'Foo::start',
            'Baz::start',
            'Bar::start',
            'Bat::start',
            'Bat::end',
            'Bar::end',
            'Baz::end',
            'Foo::end'
        ];

        $actual = new \ArrayObject([]);

        $queryBus->dispatch(
            $message = new SampleQueryWithTestContainer(10, $actual)
        );

        $this->assertEquals($expected, $actual->getArrayCopy());
    }

    public function test_it_returns_expected_result_from_handler() : void
    {
        $queryBus = new QueryBus([
            new BazSampleMiddleware(),
            new QueryDispatchingMiddleware(
                new CallableMapResolver([
                    SampleQuery::class => new SampleQueryHandler()
                ])
            ),
            new BarSampleMiddleware(),
        ]);

        $queryBus->appendMiddleware(new BatSampleMiddleware());
        $queryBus->prependMiddleware(new FooSampleMiddleware());

        $result = $queryBus->dispatch(
            $query = new SampleQuery(889)
        );

        $expected = sprintf(
            'Sample result retrieved for query of id: %d and name: %s',
            $query->id,
            $query->name
        );

        $this->assertEquals($expected, $result);
    }
}
