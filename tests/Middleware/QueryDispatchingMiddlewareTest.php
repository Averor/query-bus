<?php declare(strict_types=1);

namespace Averor\MessageBus\QueryBus\Tests\Middleware;

use Averor\MessageBus\Exception\MessageHandlerIsNotCallableException;
use Averor\MessageBus\Exception\MessageHandlerNotFoundException;
use Averor\MessageBus\QueryBus\Middleware\QueryDispatchingMiddleware;
use Averor\MessageBus\QueryBus\QueryBus;
use Averor\MessageBus\QueryBus\Tests\Fixtures\AnotherSampleQuery;
use Averor\MessageBus\QueryBus\Tests\Fixtures\AnotherSampleQueryHandler;
use Averor\MessageBus\QueryBus\Tests\Fixtures\SampleQuery;
use Averor\MessageBus\QueryBus\Tests\Fixtures\SampleQueryHandler;
use Averor\MessageBus\Resolver\CallableMapResolver;
use PHPUnit\Framework\TestCase;

/**
 * Class QueryDispatchingMiddlewareTest
 *
 * @package Averor\MessageBus\QueryBus\Tests\Middleware
 * @author Averor <averor.dev@gmail.com>
 */
class QueryDispatchingMiddlewareTest extends TestCase
{
    public function test_it_dispatches_query_to_the_proper_handler() : void
    {
        $queryBus = new QueryBus([
            new QueryDispatchingMiddleware(
                new CallableMapResolver([
                    SampleQuery::class => new SampleQueryHandler(),
                    AnotherSampleQuery::class => new AnotherSampleQueryHandler()
                ])
            )
        ]);

        $result = $queryBus->dispatch($query = new AnotherSampleQuery(3));

        $this->assertEquals(
            sprintf(
                'Another sample result retrieved for query of id: %d and name: %s',
                $query->id,
                $query->name
            ),
            $result
        );
    }

    public function test_it_dispatches_query_to_closure_handler() : void
    {
        $queryBus = new QueryBus([
            new QueryDispatchingMiddleware(
                new CallableMapResolver([
                    SampleQuery::class => new SampleQueryHandler(),
                    AnotherSampleQuery::class => function(AnotherSampleQuery $query) {
                        return sprintf("closure result for id: %d", $query->id);
                    }
                ])
            )
        ]);

        $result = $queryBus->dispatch($query = new AnotherSampleQuery(543));

        $this->assertEquals(
            sprintf("closure result for id: %d", $query->id),
            $result
        );
    }

    public function test_it_throws_exception_when_handler_cannot_be_found() : void
    {
        $queryBus = new QueryBus([
            new QueryDispatchingMiddleware(
                new CallableMapResolver([
                ])
            ),
        ]);

        $this->expectException(MessageHandlerNotFoundException::class);

        $queryBus->dispatch(new SampleQuery(3));
    }

    public function test_it_throws_exception_when_handler_is_not_callable() : void
    {
        $queryBus = new QueryBus([
            new QueryDispatchingMiddleware(
                new CallableMapResolver([
                    SampleQuery::class => 'foobar'
                ])
            ),
        ]);

        $this->expectException(MessageHandlerIsNotCallableException::class);

        $queryBus->dispatch(new SampleQuery(3));
    }
}
