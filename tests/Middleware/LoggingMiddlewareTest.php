<?php declare(strict_types=1);

namespace Averor\MessageBus\QueryBus\Tests\Middleware;

use Averor\MessageBus\QueryBus\Middleware\LoggingMiddleware;
use Averor\MessageBus\QueryBus\QueryBus;
use Averor\MessageBus\QueryBus\Tests\Fixtures\SampleQuery;
use Averor\SimpleLogger\Logger;
use PHPUnit\Framework\TestCase;
use Psr\Log\LogLevel;

/**
 * Class LoggingMiddlewareTest
 *
 * @package Averor\MessageBus\QueryBus\Tests\Middleware
 * @author Averor <averor.dev@gmail.com>
 */
class LoggingMiddlewareTest extends TestCase
{
    public function test_it_does_write_log_info() : void
    {
        $queryBus = new QueryBus([
            new LoggingMiddleware(
                new Logger('php://output'),
                LogLevel::NOTICE
            )
        ]);

        ob_start();

        $queryBus->dispatch(
            $message = new SampleQuery(10)
        );

        $result = rtrim(ob_get_clean(), PHP_EOL);

        $this->assertEquals(
            implode(PHP_EOL, [
                sprintf("notice Dispatching query %s", get_class($message)),
                sprintf("notice Query %s dispatched", get_class($message))
            ]),
            $result
        );
    }

}
